#!/usr/bin/env node
const replace = require('replace-in-file');
const changelog = process.argv.slice(2);
const jiraEndpoint = process.argv.slice(3);
let options = {
  files: changelog,
  from: /\n\n(?:Fixes\:\s)?[A-Z]+-\d+/g,
  to: (match) => {
    return " - " + match.replace("\n\n", "")
  },
};

replace.sync(options)

options = {
  files: changelog,
  from: /feat: |fix: |chore: |build: |ci: |docs: |refactor: |revert: |style: |test: /g,
  to: '',
};

replace.sync(options)

options = {
  files: changelog,
  from: /[A-Z]+-\d+/g,
  to: (match) => {
    return `[${match}](https://${jiraEndpoint}/browse/${match})`.replace("\r", "")
  },
};

replace.sync(options)
